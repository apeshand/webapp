from .envs import *


# FLASK part ↓
##########
def get_db_connection():
    conn = sqlite3.connect(db)
    conn.row_factory = sqlite3.Row
    return conn


def get_rover(rover: str):
    conn = get_db_connection()
    try:
        content = conn.execute(f'SELECT * FROM photos_{rover.lower()}').fetchall()
    except:
        abort(Response(response='Здесь тебе не рады', status=403))
    conn.close()
    if content is None:
        abort(404)
    return content


# using as a standart for 4 columns
def split_list(l: list, n: int = 4) -> list:
    for i in range(0, len(l), n):
        yield l[i:i + n]


def get_content(rover):
    content = {}
    some_content = get_rover(rover)
    cameras = list(set([x[5] for x in some_content]))
    for camera in cameras:
        data = []
        for el in some_content:
            if camera in el:
                data.append(el[:])
        if len(data) >= 4:
            splited_data = list(split_list(l=data, n=4))
        else:
            splited_data = list(split_list(l=data, n=2))
        content[camera] = splited_data
    return content


def start():
    connection = sqlite3.connect(db)
    with open(schema_file) as schema:
        connection.executescript(schema.read())
    cur = connection.cursor()
    for rover in ['perseverance', 'curiosity', 'opportunity', 'spirit']:
        content = getData(rover=rover)
        links = [x['img_src'] for x in content]
        for el in content:
            image_name = el['img_src'].split('/')[-1]
            image_path = p(PHOTOS_DIR, rover, image_name)
            image_relative_path = f"{PHOTOS_DIR_NAME}/{rover}/{image_name}"
            cur.execute(f"""INSERT INTO photos_{rover} (
                    nasa_id,
                    sol,
                    earth_date,
                    camera,
                    camera_fullname,
                    img_src,
                    img_pth
                    ) VALUES (?, ?, ?, ?, ?, ?, ?)""",
                    (
                        el['nasa_id'],
                        el['sol'],
                        el['earth_date'],
                        el['camera'],
                        el['camera_fullname'],
                        el['img_src'],
                        f"{image_relative_path}"
                    )
            )
        downloadPhotos(links=links, rover=rover)
    connection.commit()
    connection.close()
##########
# get content for app part ↓
##########
async def downloadImages(session, path: p, url: str):
    try:
        resp = await session.request(method="GET", url=url)
    except Exception as ex:
        print(ex)
        return
    if not path.exists():
        if resp.status == 200:
            async with aiofiles.open(path, 'wb') as image:
                await image.write(await resp.read())
        else:
            print(f"Error! Response {resp.status} not equal '200'.")
    elif path.exists():
        print(f"Image {str(path).split('/')[-1]} exists.")


async def generateRequests(urls: list, rover: str):
    async with ClientSession() as session:
        tasks = []
        for url in urls:
            image_name = url.split('/')[-1]
            imageAbsolutePath = p(PHOTOS_DIR, rover, image_name)
            tasks.append(
                downloadImages(session, path=imageAbsolutePath, url=url )
            )
        await asyncio.gather(*tasks)


def downloadPhotos(links: list, rover: str):
    if not PHOTOS_DIR.exists():
        PHOTOS_DIR.mkdir()

    if not p(PHOTOS_DIR, rover).exists():
        p(PHOTOS_DIR, rover).mkdir()

    asyncio.run(generateRequests(urls=links, rover=rover))
### download photos part ↑


def parseResponse(response: list) -> dict:
    photos = []
    for photo in response:
        temp_dict = {}
        temp_dict['nasa_id'] = photo['id']
        temp_dict['sol'] = photo['sol']
        temp_dict['camera'] = photo['camera']['name']
        temp_dict['camera_fullname'] = photo['camera']['full_name']
        temp_dict['img_src'] = photo['img_src']
        temp_dict['earth_date'] = photo['earth_date']
        photos.append(temp_dict)
    return photos


def getContent(rover: str, apilink:str) -> dict:
    content = requests.get(apilink).json()
    return content


def getData(rover: str) -> list:
    APILINK = f"{NASA_API}/{rover}/latest_photos?api_key={APIKEY}"
    content = getContent(rover=rover, apilink=APILINK)
    content = list(*dict(content).values())
    # days before last photo date
    tDelta = 3
    earth_date = dt.date.fromisoformat(content[0]['earth_date'])
    for day in [(earth_date - dt.timedelta(i)).isoformat() for i in range(1, tDelta + 1)]:
        _APILINK = f"{NASA_API}/{rover}/photos?api_key={APIKEY}&earth_date={day}"
        _content = getContent(rover=rover, apilink=_APILINK)
        content = content + list(*dict(_content).values())
    print(f"Total photos for {rover}: ", len(content))
    try:
        return parseResponse(content)
    except:
        print(f"Shit happens!\ncontent: {json.dumps(content)}")

##########