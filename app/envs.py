from os import getenv as ge
from pathlib import Path as p
from werkzeug.exceptions import abort
from werkzeug.wrappers import Response
import requests
import json
import sqlite3
import asyncio
from aiohttp import ClientSession
import aiofiles
import datetime as dt


# Корневая папка приложения
BASE_DIR = p(__file__).parents[1]

# NASA API URL
NASA_API = "https://api.nasa.gov/mars-photos/api/v1/rovers"

# DB section
DB_DIR = p(BASE_DIR, 'db')
db = p(DB_DIR, 'database.sqlite')
schema_file = p(DB_DIR, 'schema.sql')

# Photos dir
PHOTOS_DIR_NAME = "photos"
PHOTOS_DIR = p(BASE_DIR, 'static', PHOTOS_DIR_NAME)

# ENVs section for NASA API for Mars
APIKEY    = ge("APIKEY",    default="DEMO_KEY" )
ROVER     = ge("ROVER",     default="curiosity")
SOL       = ge("SOL",       default=""         )
DATE      = ge("DATE",      default=""         )
CAMERA    = ge("CAMERA",    default="FHAZ"     )
NUM_IMG   = ge("NUM_IMG",   default="20"       )
