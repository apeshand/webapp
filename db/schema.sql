DROP TABLE IF EXISTS photos_perseverance;
DROP TABLE IF EXISTS photos_curiosity;
DROP TABLE IF EXISTS photos_opportunity;
DROP TABLE IF EXISTS photos_spirit;

CREATE TABLE photos_perseverance (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    nasa_id INTEGER NOT NULL,
    sol INTEGER NOT NULL,
    earth_date TEXT NOT NULL,
    camera TEXT NOT NULL,
    camera_fullname TEXT NOT NULL,
    img_src TEXT NOT NULL,
    img_pth TEXT NOT NULL
);
CREATE TABLE photos_curiosity (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    nasa_id INTEGER NOT NULL,
    sol INTEGER NOT NULL,
    earth_date TEXT NOT NULL,
    camera TEXT NOT NULL,
    camera_fullname TEXT NOT NULL,
    img_src TEXT NOT NULL,
    img_pth TEXT NOT NULL
);
CREATE TABLE photos_opportunity (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    nasa_id INTEGER NOT NULL,
    sol INTEGER NOT NULL,
    earth_date TEXT NOT NULL,
    camera TEXT NOT NULL,
    camera_fullname TEXT NOT NULL,
    img_src TEXT NOT NULL,
    img_pth TEXT NOT NULL
);
CREATE TABLE photos_spirit (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    nasa_id INTEGER NOT NULL,
    sol INTEGER NOT NULL,
    earth_date TEXT NOT NULL,
    camera TEXT NOT NULL,
    camera_fullname TEXT NOT NULL,
    img_src TEXT NOT NULL,
    img_pth TEXT NOT NULL
);