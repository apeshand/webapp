from flask import Flask, render_template
from app.funcs import *
from multiprocessing import Process


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/<string:rover>')
def rover(rover):
    content = get_content(rover)
    return render_template('rover.html', rover=rover, cameras=content)


def main():
    app.run(debug=True, use_reloader=True, host="0.0.0.0")


match __name__:
    case "__main__":
        if not db.exists():
            procInit = Process(target = start)
            procMain = Process(target = main)
            procInit.start(); procMain.start()
            procInit.join(); procMain.join()
        else:
            main()
    case _:
        print("Error!")
        exit(1)