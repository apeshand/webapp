function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
  // Это ломаете тебе Darkmode!!!
  //document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft = "0";
  // Это ломаете тебе Darkmode!!!
  //document.body.style.backgroundColor = "white";
}

/* Это плавное скрытие/открытие твоего блока (используется jQuery) */
function diplay_hide (myDIV)
{
  if ($(myDIV).css('display') == 'none')
  {
    $(myDIV).animate({height: 'show'}, 500);
  }
  else
  {  
    $(myDIV).animate({height: 'hide'}, 500);
  }
}

function darkMode() {
  var element = document.body;
  element.classList.toggle("dark-mode");
}